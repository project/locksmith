# Locksmith

The goal of this module is to facilitate the access, storage and
management of credentials from third party services. For a lot of
projects, you will be given keys and credentials from multiple services
that might not have an existing Drupal module.

Hopefully this module will save you time by preventing you from having
to create a custom form page with permissions only to save two access
keys that were given to you by a third party you are working with.

This module **does not** contain any functionality regarding
authentication. You will have to do it yourself or by using another
contrib module.

## How it works

### Add credentials to the site

All credentials can be managed through Drupal's user interface.

1. Go to the module's administration page in
*Admin > Configuration > Webservices > Locksmith*
2. Set the name of the service associated to the credentials
3. Add all the credentials that the third party gave you
4. Export the configurations.

### Accessing the credentials in your code

The credentials can be accessed by using the _Locksmith_ service.

Given that you have a service called `foo` with a credential named
`bar`, you can then get its value by using the following line :

```
\Drupal::service('locksmith.credentials')->get('foo')->get('bar');
```

## Recommendations

### Use dependency injection
Before using the _Locksmith_ service, we highly advise you to use
dependency injection whenever possible. You can read about the subject
and the benefits
[here](https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8).

### Use the _config split_ module
Your site will probably be installed on multiple environment during
its lifetime (eg. QA or Production). This means that you will possibly
have credentials that only work on a specific environment or IP address.
To solve this issue, we recommend you to use the
[Config Split module](https://www.drupal.org/project/config_split). It will
allow you to modify the credentials based on which environment the website
is currently on.
