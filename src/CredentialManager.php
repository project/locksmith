<?php

namespace Drupal\locksmith;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\locksmith\Entity\CredentialEntity;
use Drupal\locksmith\Entity\CredentialEntityInterface;

/**
 * Class CredentialManager service.
 *
 * @package Drupal\locksmith
 */
class CredentialManager {

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * CredentialManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory service.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config;
  }

  /**
   * Retrieves a locksmith credential group by it's ID.
   *
   * @param string $id
   *   Unique identifier of the locksmith credential group.
   *
   * @return \Drupal\locksmith\Entity\CredentialEntityInterface
   *   The locksmith credential entity.
   */
  public function get($id) : CredentialEntityInterface {
    $credential = CredentialEntity::load($id);

    if ($credential === NULL) {
      throw new \InvalidArgumentException("Locksmith could not find the credentials : '$id'");
    }

    return $credential;
  }

}
