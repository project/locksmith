<?php

namespace Drupal\locksmith\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\locksmith\Entity\CredentialEntity;

/**
 * Class ConfigSplitEntityForm.
 *
 * @package Drupal\config_split\Form
 */
class CredentialEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $formState) {
    $form = parent::form($form, $formState);
    $form['#attached']['library'][] = 'locksmith/form';
    $form['#tree'] = TRUE;

    // Get the current values of the credential entity.
    /** @var \Drupal\locksmith\Entity\CredentialEntity $config */
    $config = $this->entity;

    // General information.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#description' => $this->t("Name of the service or third party providing the credentials"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\locksmith\Entity\CredentialEntity::load',
      ],
    ];

    // Initial number of credentials.
    $credentialsValues = $this->getCredentialValues($config);
    $formState->set('credential_rows', $formState->get('credential_rows') ?? range(0, max(1, count($credentialsValues) - 1)));

    // Container for our repeating fields.
    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Credentials'),
      '#tree' => TRUE,
      '#prefix' => '<div id="js-ajax-elements-wrapper">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => ['credentials-container'],
      ],
    ];

    // Add our names fields.
    foreach ($formState->get('credential_rows') as $delta) {
      $form['credentials']['rows'][$delta] = [
        '#type' => 'container',
        '#title' => $this->t('Label'),
        '#attributes' => [
          'class' => ['credentials-row'],
        ],
      ];

      $form['credentials']['rows'][$delta]['name'] = [
        '#type' => 'container',
        '#title' => $this->t('Label'),
        '#attributes' => [
          'class' => ['credentials-col', 'credentials--name'],
        ],
      ];

      $form['credentials']['rows'][$delta]['name']['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $credentialsValues[$delta]['label'] ?? '',
        '#required' => FALSE,
        '#wrapper_attributes' => [
          'class' => ['credentials-field', 'credentials-field--label'],
        ],
      ];

      $form['credentials']['rows'][$delta]['name']['key'] = [
        '#type' => 'machine_name',
        '#title' => $this->t('Machine name:'),
        '#default_value' => $credentialsValues[$delta]['key'] ?? '',
        '#description' => '',
        '#required' => FALSE,
        '#machine_name' => [
          'exists' => [$this, 'validateCredentialKey'],
          'source' => ['credentials', 'rows', $delta, 'name', 'label'],
        ],
        '#wrapper_attributes' => [
          'class' => ['credentials-field', 'credentials-field--key'],
        ],
      ];

      $form['credentials']['rows'][$delta]['value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Value'),
        '#default_value' => $credentialsValues[$delta]['value'] ?? '',
        '#required' => FALSE,
        '#wrapper_attributes' => [
          'class' => [
            'credentials-col',
            'credentials-field',
            'credentials-field--value',
          ],
        ],
      ];

      // Button that removes the credential row.
      $form['credentials']['rows'][$delta]['remove'] = [
        '#type' => 'submit',
        '#name' => "remove-$delta",
        '#value' => $this->t('Remove'),
        '#delta' => $delta,
        '#submit' => [
          [$this, 'removeRow'],
        ],
        '#ajax' => [
          'callback' => [$this, 'updateCredentialsCallback'],
          'effect' => 'fade',
          'wrapper' => 'js-ajax-elements-wrapper',
          'progress' => ['type' => 'none'],
        ],
        '#attributes' => [
          'class' => ['credentials-remove'],
        ],
      ];
    }

    // Button that creates a new credential row.
    $form['credentials']['add'] = [
      '#type' => 'submit',
      '#name' => 'add',
      '#value' => $this->t('Add'),
      '#submit' => [
        [$this, 'addRow'],
      ],
      '#ajax' => [
        'callback' => [$this, 'updateCredentialsCallback'],
        'effect' => 'fade',
        'wrapper' => 'js-ajax-elements-wrapper',
        'progress' => ['type' => 'none'],
      ],
    ];

    return $form;
  }

  /**
   * Add credential button callback.
   *
   * @param array $form
   *   The form object.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object.
   */
  public function addRow(array &$form, FormStateInterface &$formState): void {
    $deltas = $formState->get('credential_rows');
    $deltas[] = max($deltas) + 1;
    $formState->set('credential_rows', $deltas);
    $formState->setRebuild(TRUE);
  }

  /**
   * Remove credential button callback.
   *
   * @param array $form
   *   The form object.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object.
   */
  public function removeRow(array &$form, FormStateInterface &$formState): void {
    $delta = $formState->getTriggeringElement()['#delta'];
    $deltas = $formState->get('credential_rows');
    $index = array_search($delta, $deltas);
    unset($deltas[$index]);

    $formState->set('credential_rows', $deltas);

    if (empty($deltas)) {
      $formState->set('credential_rows', [$delta + 1]);
    }

    // Update the number of item.
    $currentCount = $formState->get('credential_count');
    $formState->set('credential_count', max(1, $currentCount - 1));
    $formState->setRebuild(TRUE);
  }

  /**
   * Update credentials callback.
   *
   * @param array $form
   *   The form object.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object.
   *
   * @return array
   *   The form element being changed.
   */
  public function updateCredentialsCallback(array &$form, FormStateInterface &$formState): array {
    return $form['credentials'];
  }

  /**
   * Fetches the credential values and format them accordingly.
   *
   * @param \Drupal\locksmith\Entity\CredentialEntity $entity
   *   The entity being created or updated.
   *
   * @return array
   *   The formatted credentials.
   */
  protected function getCredentialValues(CredentialEntity $entity) {
    $credentials = $entity->getCredentials();
    $values = [];

    foreach ($credentials as $key => $properties) {
      $values[] = [
        'key' => $key,
        'label' => $properties['label'],
        'value' => $properties['value'],
      ];
    }

    return $values;
  }

  /**
   * Makes sure that a credential key is not set more than once.
   *
   * @param string $value
   *   The submitted value.
   * @param array $element
   *   The form element array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object.
   *
   * @return string|null
   *   Returns null of there is no duplicated key.
   */
  public function validateCredentialKey($value, array $element, FormStateInterface $formState): ?string {
    $keys = array_count_values(self::getCredentialKeys($formState));

    // Return the value if it appears more than once. Which will cause an error
    // in the field.
    if ($keys[$value] > 1) {
      return $element['#value'];
    }

    // Return nothing, which means that the key is safe to use.
    return NULL;
  }

  /**
   * Retrieves all credential keys that are submitted in the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Current form state.
   *
   * @return string[]
   *   The list of credential keys in the form state.
   */
  public static function getCredentialKeys(FormStateInterface $formState): array {
    $keys = [];
    $formValues = $formState->getValues();
    foreach ($formValues['credentials']['rows'] as $credentialFields) {
      $keys[] = $credentialFields['name']['key'] ?? NULL;
    }

    return $keys;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $formState) {
    parent::save($form, $formState);
    $formState->setRedirect('entity.locksmith.collection');
    $this->messenger()
      ->addStatus($this->t('Successfully updated the credentials of %label', [
        '%label' => $this->entity->label(),
      ]));
  }

  /**
   * {@inheritdoc}
   */
  public function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $formState) {
    parent::copyFormValuesToEntity($entity, $form, $formState);
    $formValues = $formState->getValues();
    $credentials = [];

    foreach ($formValues['credentials']['rows'] as $credentialFields) {
      if ($credentialFields['name']['key']) {
        $credentials[$credentialFields['name']['key']] = [
          'label' => $credentialFields['name']['label'],
          'value' => $credentialFields['value'],
        ];
      }
    }

    if ($entity instanceof ConfigEntityInterface) {
      $entity->set('credentials', $credentials);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    $formValues = $formState->getValues();

    // Validate the credentials fields.
    foreach ($formValues['credentials']['rows'] as $delta => $credentialRow) {
      // Check that the credential row has a label if it has a key.
      if (!empty($credentialRow['name']['key']) && empty($credentialRow['name']['label'])) {
        $faultyField = "credentials][rows][$delta][name][label";
        $formState->setErrorByName($faultyField, $this->t('A credential has to have a label'));
      }

      // Check that the credential row has a key if it has a label.
      if (!empty($credentialRow['name']['label']) && empty($credentialRow['name']['key'])) {
        $faultyField = "credentials][rows][$delta][name][key";
        $formState->setErrorByName($faultyField, $this->t('A credential has to have a key'));
      }

      // Check that the credential row has a key if it has a value.
      if (!empty($credentialRow['value']) && empty($credentialRow['name']['key'])) {
        $faultyField = "credentials][rows][$delta][value";
        $formState->setErrorByName($faultyField, $this->t('A value cannot be set if there is no key'));
      }
    }

    parent::validateForm($form, $formState);
  }

}
