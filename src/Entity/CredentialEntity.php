<?php

namespace Drupal\locksmith\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Lock smith credential entity.
 *
 * @noinspection AnnotationMissingUseInspection
 *
 * @ConfigEntityType(
 *   id = "locksmith",
 *   label = @Translation("Credentials"),
 *   handlers = {
 *     "list_builder" = "Drupal\locksmith\Entity\CredentialEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\locksmith\Form\CredentialEntityForm",
 *       "edit" = "Drupal\locksmith\Form\CredentialEntityForm",
 *       "delete" = "Drupal\locksmith\Form\CredentialEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "credentials",
 *   admin_permission = "administer locksmith credentials",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/services/locksmith/credential/add",
 *     "edit-form" = "/admin/config/services/locksmith/credential/{locksmith}/edit",
 *     "delete-form" = "/admin/config/services/locksmith/credential/{locksmith}/delete",
 *     "collection" = "/admin/config/services/locksmith/credential"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "credentials",
 *   }
 * )
 */
class CredentialEntity extends ConfigEntityBase implements CredentialEntityInterface {

  /**
   * The locksmith credential unique identifier.
   *
   * @var string
   */
  protected $id;

  /**
   * The name of the service providing the credentials.
   *
   * @var string
   */
  protected $label;

  /**
   * The list of credentials provided to the service.
   *
   * @var array
   */
  protected $credentials = [];

  /**
   * {@inheritdoc}
   */
  public function getCredentials(): array {
    return $this->credentials ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getKeys(): array {
    return array_keys($this->credentials) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function get($propertyName) {
    $credentials = $this->getCredentials();
    return isset($credentials[$propertyName]) ? $credentials[$propertyName]['value'] : parent::get($propertyName);
  }

}
