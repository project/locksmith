<?php

namespace Drupal\locksmith\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Configuration Split Setting entities.
 */
interface CredentialEntityInterface extends ConfigEntityInterface {

  /**
   * Fetches all credential of that service.
   *
   * @return array
   *   Keyed credentials of the service.
   */
  public function getCredentials() : array;

  /**
   * Fetches all available credentials in the service.
   *
   * @return array
   *   List of credential keys with a value available.
   */
  public function getKeys() : array;

}
